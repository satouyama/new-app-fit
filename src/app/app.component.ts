import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateService } from '@ngx-translate/core';
import { Global } from '../providers/global';

@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  rootPage:any = 'LoginPage';
  

  constructor(public platform: Platform,public global:Global, statusBar: StatusBar, splashScreen: SplashScreen, public translate: TranslateService) {
    translate.setDefaultLang(this.global.language);

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
    this.global.changeLanguage(this.global.language);
  
  }  

   // animate my app Function
   public animateVarible:boolean=false;  
}
