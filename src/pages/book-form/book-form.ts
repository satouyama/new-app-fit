import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-book-form',
  templateUrl: 'book-form.html',
})
export class BookFormPage {
  tabBarElement: any;
  public event = {
    month: '2018-05-19',
    timeStarts: '07:43',
  }
  constructor(public navCtrl: NavController, public navParams: NavParams, public translate: TranslateService) {
    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
  }
 
  //hide tabs 
  ionViewWillEnter() {
     this.tabBarElement.style.display = 'none';
  }  
    
  ionViewWillLeave() {   
     this.tabBarElement.style.display = 'flex';
  }
}
