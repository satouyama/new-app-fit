import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BookFormPage } from './book-form';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    BookFormPage,
  ],
  imports: [
    IonicPageModule.forChild(BookFormPage),TranslateModule.forChild()
  ],
})
export class BookFormPageModule {}
