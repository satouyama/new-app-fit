import { Component, ViewChild, ElementRef  } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
declare var google;

@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {
  tabBarElement: any;
  map: any; 
  markerBounds: any;
  active_status=3;

  @ViewChild('map') mapElement: ElementRef;  

  constructor(public navCtrl: NavController, public navParams: NavParams, public translate: TranslateService) {
    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
  }

  //hide tabs 
  ionViewWillEnter() {
    this.tabBarElement.style.display = 'none';
  }  

  ionViewWillLeave() {   
    this.tabBarElement.style.display = 'flex';
  }

  // map functions
  ionViewDidLoad(){
    this.initMap();
    
  }
  initMap() {   
    this.map = new google.maps.Map(this.mapElement.nativeElement, {
      zoom: 7,
      center: {lat: -15.7801, lng: -47.9292 }
    });
   
      this.addMarker();      
    
  }
  addMarker(){
    var icon = {
      url: "assets/imgs/pin.png", // url
      scaledSize: new google.maps.Size(25, 35), // scaled size
    };
    var icon2 = {
      url: "assets/imgs/point.png", // url
      scaledSize: new google.maps.Size(20, 20), // scaled size
    };

    var markerdata ={
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: this.map.getCenter()
      ,icon:icon 
    }
    let marker = new google.maps.Marker(markerdata);
    marker.setMap(this.map);

    var markerdata2 ={
      map: this.map,
      animation: google.maps.Animation.DROP,
      position: {lat: 41.95, lng: -87.65}
      ,icon:icon2 
    }
    let marker2 = new google.maps.Marker(markerdata2);
    marker2.setMap(this.map);
    }  

}
