import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { Global } from "../../providers/global";

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {
  list: Array<any>;
  activeSelect;

  constructor(public navCtrl: NavController, public navParams: NavParams, public translate: TranslateService, public global: Global) {
    this.list = [
    { img: "assets/imgs/01.png", title_en:"Personais", title_ar: "الأطباء", component: "SecondListPage" },
    { img: "assets/imgs/03.png", title_en: "Treinos", title_ar: "الصيدليات", component: "TreinosPage" }, 
    { img: "assets/imgs/04.png", title_en: "Meus Clientes", title_ar: "العيادات", component: "FirstListPage" }, 
    { img: "assets/imgs/06.png", title_en: "Questionários", title_ar: "مقالات", component: "ArticleListPage" }
  
  ]
  }
  //go to page
  goTo(index) {
    this.navCtrl.push(this.list[index].component);
  }

  //active item
  activeItem(index) {
    this.activeSelect = index;
  }
}
