import { Component } from '@angular/core';
import { IonicPage  } from 'ionic-angular';

@IonicPage()
@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = 'HomePage';
  tab3Root = 'FavoritesPage';
  tab4Root = 'ProfilePage';
  tab5Root = 'ChatListPage';
  tab6root = 'SettingsPage'; 

  constructor() {}
}
