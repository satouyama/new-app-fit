import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { Global } from "../../providers/global";

@IonicPage()
@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
})
export class DetailsPage {
rate=4.5;  
ratenow=0; 
tabBarElement: any;
list:Array<any>;

  constructor(public navCtrl: NavController, public navParams: NavParams, public translate: TranslateService, public global: Global) {
     this.tabBarElement = document.querySelector('.tabbar.show-tabbar');

     this.list=[{day_en:"Segunda", day_ar:"الإثنين", times:"8:30  - 16:00 PM"},]
  }
   
   //hide tabs 
   ionViewWillEnter() {
    this.tabBarElement.style.display = 'none';
  }  

  ionViewWillLeave() {   
    this.tabBarElement.style.display = 'flex';
  }

}
