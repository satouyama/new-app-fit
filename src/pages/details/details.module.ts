import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailsPage } from './details';
import { TranslateModule } from '@ngx-translate/core';
import { Ionic2RatingModule } from "ionic2-rating";

@NgModule({
  declarations: [
    DetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailsPage),TranslateModule.forChild(),Ionic2RatingModule
  ],
})
export class DetailsPageModule {}
