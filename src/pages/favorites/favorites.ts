import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { Global } from "../../providers/global";
import { Storage } from '@ionic/storage';
@IonicPage()
@Component({
  selector: 'page-favorites',
  templateUrl: 'favorites.html',
})
export class FavoritesPage {
  list:Array<any>;
   
  constructor(public navCtrl: NavController, public navParams: NavParams, public translate: TranslateService, public global: Global) {
    this.list=[
    {img:"https://cdn.dribbble.com/users/432077/screenshots/2789560/pt-dribbble3.jpg",name_en:"James Richardson",name_ar:"اسم الطبيب", degree_en:"Natação ", degree_ar:"دكتوراه في طب الأطفال", address_en:"Brasília, DF" , rate  : "2"},
    {img:"https://cdn.dribbble.com/users/432077/screenshots/2789560/pt-dribbble3.jpg",name_en:"Yuri Tavares",name_ar:"اسم المستشفى", degree_en:"General services", degree_ar:"خدمات عامة", address_en:"Brasília, DF", address_ar:"محمد بن راشد ، دبي ، الإمارات العربية المتحدة", rate:"3"}]
    
  }

  //delete item
  deleteItem(item,ev) {
    this.list.splice(this.list.indexOf(item), 1);
    ev.stopPropagation();
  } 

}  
