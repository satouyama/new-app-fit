import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FavoritesPage } from './favorites';
import { TranslateModule } from '@ngx-translate/core';
import { Ionic2RatingModule } from "ionic2-rating";

@NgModule({
  declarations: [
    FavoritesPage,
  ],
  imports: [
    IonicPageModule.forChild(FavoritesPage),TranslateModule.forChild(),Ionic2RatingModule
  ],
})
export class FavoritesPageModule {}
