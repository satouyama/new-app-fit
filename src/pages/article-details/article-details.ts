import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { Global } from "../../providers/global";

@IonicPage()
@Component({
  selector: 'page-article-details',
  templateUrl: 'article-details.html',
})
export class ArticleDetailsPage {
  tabBarElement: any;
  public details_title;
  public details_content;

  constructor(public navCtrl: NavController, public navParams: NavParams, public translate: TranslateService, public global: Global) {
  this.tabBarElement = document.querySelector('.tabbar.show-tabbar');
  this.details_title={title_ar:"أخر أخبار الصحة والطب",title_en:"Health & Medicine News title"}
  this.details_content={content_ar:"الارشادات الطبية ، علاج للأمراض ، اخر اخبار الطب والصحة فى العالم يشمل نصائح طبية، وتغطية للاكتشافات العلمية فى عالم الطب والصحة العامة،الارشادات الطبية ، علاج للأمراض ، اخر اخبار الطب والصحة فى العالم يشمل نصائح طبية، وتغطية للاكتشافات العلمية فى عالم الطب والصحة العامة"
  ,content_en:"is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially  unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."}
  console.log(this.global.language);
}

  //hide tabs 
  ionViewWillEnter() {
  this.tabBarElement.style.display = 'none';
  console.log(this.details_content);
  }    


  ionViewWillLeave() {   
  this.tabBarElement.style.display = 'flex';
  }

}
