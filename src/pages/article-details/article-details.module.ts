import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ArticleDetailsPage } from './article-details';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ArticleDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(ArticleDetailsPage),TranslateModule.forChild()
  ],
})
export class ArticleDetailsPageModule {}
