import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Global } from "../../providers/global";

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {   
  loginForm: FormGroup; 
  data = { nickname:"DougFan" };
  constructor(public navCtrl: NavController, public navParams: NavParams, public translate: TranslateService, public global: Global, public formBuilder: FormBuilder) {

  // Email And Password Validation
  this.loginForm = this.formBuilder.group({
    name: [
        '', Validators.compose([
        Validators.maxLength(20),
        Validators.pattern('[a-zA-Z ]*'),
        Validators.required
      ])
    ],
    email: [   
      '', Validators.compose([
        Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/),
        Validators.required
      ])
    ],
    password: [
      '', Validators.compose([
        Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/),
        Validators.required
      ])
    ],
    });
  }

}
