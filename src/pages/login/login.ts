import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Global } from "../../providers/global";

@IonicPage()   
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  loginForm: FormGroup; 

  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController,
     public translate: TranslateService, public global: Global, public formBuilder: FormBuilder,private toastCtrl: ToastController) {   

    // name And Password Validation
    this.loginForm = this.formBuilder.group({
      name: [
          '', Validators.compose([
          Validators.maxLength(20),
          Validators.pattern('[a-zA-Z ]*'),
          Validators.required
        ])
      ],
      password: [
        '', Validators.compose([
          Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/),
          Validators.required
        ])
      ],
    });

  }

  forgotPassword() {
    let forgotpas = this.alertCtrl.create({
      title: 'Forgot password',
      message: "Enter your email address and we'll help you reset your password",
      inputs: [
        {
          name: 'email',
          placeholder: 'E-mail'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Send',
          handler: data => {
            if (!data.email) 
            {
              this.showErrorToast("please enter Email")
              return false;
            }else if(!this.mailFormat(data.email)){ 
              this.showErrorToast("Email is invaild")
              return false;
            }else{
              console.log('Send clicked');
            }
          }
        }
      ]
    });
    forgotpas.present();
  }

  mailFormat(email) {
    var EMAIL_REGEXP = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
    console.log(EMAIL_REGEXP.test(email))
    if(email.length > 5 && EMAIL_REGEXP.test(email))   return true ;
    else return false;
  } 

  showErrorToast(txt) {
    let toast = this.toastCtrl.create({ message: txt,duration: 3000, position: 'center' });
    toast.present();
  }

}
