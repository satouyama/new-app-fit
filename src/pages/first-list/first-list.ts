import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { Global } from "../../providers/global";

@IonicPage()
@Component({
  selector: 'page-first-list',
  templateUrl: 'first-list.html',
})
export class FirstListPage {
  tabBarElement: any;
  list:Array<any>;
  activeSelect;

  constructor(public navCtrl: NavController, public navParams: NavParams, public translate: TranslateService, public global: Global) {
    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');

    this.list=[
    {img:"assets/imgs/09.png",title_en:"Cardio Trainer",title_ar:"قلب"},

  ]
  }
  
  //hide tabs 
  ionViewWillEnter() {
    this.tabBarElement.style.display = 'none';
  }  

  ionViewWillLeave() {   
    this.tabBarElement.style.display = 'flex';
  }
  //active item
  activeItem(index){
    this.activeSelect=index;
  }
}
