import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FirstListPage } from './first-list';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    FirstListPage,
  ],
  imports: [
    IonicPageModule.forChild(FirstListPage),TranslateModule.forChild()
  ],
})
export class FirstListPageModule {}
