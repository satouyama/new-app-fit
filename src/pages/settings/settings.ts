import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { MyApp } from '../../app/app.component';
import { Global } from "../../providers/global";

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public _myApp:MyApp, public translate: TranslateService, public app: App, public global: Global) {
  }

  // animate Function
  animateApp(e:any){ 
    this._myApp.animateVarible = e.checked;
  }

  //logout
  logout(){   
     this.app.getRootNav().setRoot('LoginPage');
  }
}
