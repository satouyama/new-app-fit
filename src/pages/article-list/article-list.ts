import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { Global } from "../../providers/global";

@IonicPage()
@Component({
  selector: 'page-article-list',
  templateUrl: 'article-list.html',
})
export class ArticleListPage {
  tabBarElement: any;
  list:Array<any>;

  constructor(public navCtrl: NavController, public navParams: NavParams, public translate: TranslateService, public global: Global) {
    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');

    this.list=[{img:"https://cdn-images-1.medium.com/max/2600/1*U7srFtvjddOHm8nGoeA2KQ.jpeg",title_en:"Treino",title_ar:"أخر أخبار الصحة والطب"},
]
 }
  
  //hide tabs 
  ionViewWillEnter() {
    this.tabBarElement.style.display = 'none';
  }  

  ionViewWillLeave() {   
    this.tabBarElement.style.display = 'flex';
  }
}
