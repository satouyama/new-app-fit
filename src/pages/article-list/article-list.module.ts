import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ArticleListPage } from './article-list';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    ArticleListPage,
  ],
  imports: [
    IonicPageModule.forChild(ArticleListPage),TranslateModule.forChild()
  ],
})
export class ArticleListPageModule {}
