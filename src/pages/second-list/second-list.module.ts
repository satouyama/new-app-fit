import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SecondListPage } from './second-list';
import { TranslateModule } from '@ngx-translate/core';
import { Ionic2RatingModule } from "ionic2-rating";

@NgModule({
  declarations: [
    SecondListPage,
  ],
  imports: [
    IonicPageModule.forChild(SecondListPage),TranslateModule.forChild(),Ionic2RatingModule
  ],
})
export class SecondListPageModule {}
