import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { Global } from "../../providers/global";

@IonicPage()
@Component({
  selector: 'page-second-list',
  templateUrl: 'second-list.html',
})
export class SecondListPage {
  tabBarElement: any;
  list:Array<any>;

  constructor(public navCtrl: NavController, public navParams: NavParams, public translate: TranslateService, public global: Global) {
    this.tabBarElement = document.querySelector('.tabbar.show-tabbar');

    this.list=[{img:"https://blog.agenciadosite.com.br/wp-content/uploads/2017/02/logo-para-personal-03.png",name_en:"Rafael Alves",name_ar:"اسم الطبيب", degree_en:"Natação", degree_ar:"دكتوراه في طب الأطفال", address_en:"Brasília , DF", address_ar:"محمد بن راشد ، دبي ، الإمارات العربية المتحدة", rate:"5"},
]
  }
  
  //hide tabs 
  ionViewWillEnter() {
    this.tabBarElement.style.display = 'none';
  }  

  ionViewWillLeave() {   
    this.tabBarElement.style.display = 'flex';
  }

   // save item
   saveItem(item,ev){
    item.saveThis = !item.saveThis;
    ev.stopPropagation();
  }
}
