import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TreinosPage } from './treinos';

@NgModule({
  declarations: [
    TreinosPage,
  ],
  imports: [
    IonicPageModule.forChild(TreinosPage),
  ],
})
export class TreinosPageModule {}
