import { Injectable } from "@angular/core";
import { Platform } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';

@Injectable()  
export class Global  {
  language='en';
  public constructor( public translate: TranslateService,public platform: Platform){}

  //change language
  changeLanguage(lng) {
    console.log(lng);
    this.language=lng
    this.translate.use(lng);
    if(lng=='ar'){
      this.platform.setDir('rtl', true);
    }
    else {
      this.platform.setDir('ltr', true);
    }
  }
  
};     